# Bootstrap Navbar Center

Adds centered navbar behavior to bootstrap.

Installing with bower:

Run `bower i --save bootstrap-navbar-center=git@bitbucket.org:Battousai/bootstrap-navbar-center.git`
in your terminal
